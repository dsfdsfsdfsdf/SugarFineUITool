﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="highlightjs.aspx.cs" Inherits="WebTest.highlightjs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
<%--    <script src="jquery-3.3.1.min.js"></script>--%>
    <script src="jquery-1.11.3.min.js"></script>
    <script src="highlight.pack.js"></script>
    <link href="styles/vs.css" rel="stylesheet" />
    <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <pre> 
        <code >
            
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTest
{
    public partial class highlightjs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
        </code> 
</pre>
        </div>
    </form>
</body>
</html>

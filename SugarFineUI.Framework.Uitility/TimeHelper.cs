﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SugarFineUI.Framework.Uitility
{
    public class TimeHelper
    {
        /// <summary>
        /// 和当前时间比对，获取过去的时间内容
        /// </summary>
        /// <param name="beginTime">要对比的时间</param>
        /// <returns></returns>
        public static string GetPastTimeText(DateTime beginTime)
        {
            //如果刷新的时间大于当前时间，说明是提前刷新。算是一天前
            if (beginTime > DateTime.Now) beginTime= beginTime.AddDays(-1);

            return GetPastTimeText(beginTime, DateTime.Now) ;
        }

        /// <summary>
        /// 开始和结束时间对比，获取过去的时间内容
        /// </summary>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束</param>
        /// <returns></returns>
        public static string GetPastTimeText(DateTime beginTime, DateTime endTime )
        {
            TimeSpan ts = endTime- beginTime;

            return  GetPastTimeText(ts) ;
        }

        /// <summary>
        /// 获取过去的时间内容
        /// </summary>
        /// <param name="ts">时间差</param>
        /// <returns></returns>
        public static string GetPastTimeText(TimeSpan ts)
        {
            if (ts.TotalDays > 365)
            {
                return Math.Round(ts.TotalDays / 365) + "年前";
            }
            else if (ts.TotalDays > 30)
            {
                return Math.Round(ts.TotalDays / 30) + "月前";
            }
            else if (ts.TotalDays > 1)
            {
                return Math.Round(ts.TotalDays) + "天前";
            }
            else if (ts.TotalHours > 1)
            {
                return Math.Round(ts.TotalHours) + "小时前";
            }
            else if (ts.TotalMinutes > 1)
            {
                return Math.Round(ts.TotalMinutes) + "分钟前";
            }
            else
            {
                //return Math.Round(ts.TotalSeconds) + "秒前";
                return "刚刚发表" ;
            }
        }
    }
}

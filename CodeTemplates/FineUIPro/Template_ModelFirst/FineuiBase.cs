﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template_ModelFirst
{
    public class FineuiBase
    {
        /// <summary>
        /// 获取备注信息，没有备注则显示表名称
        /// </summary>
        /// <param name="comment">备注信息</param>
        /// <param name="name">表名称</param>
        /// <returns></returns>
        public static string GetComment(string comment, string name)
        {
            return string.IsNullOrEmpty(comment) ? name : comment ;
        }

        public static string GetNamespace2Str(string namespace2Str)
        {
            return namespace2Str.Length == 0 ? string.Empty : namespace2Str + "." ;
        }

        /// <summary>
        /// 过滤可null类型
        /// </summary>
        /// <param name="typeName">类型名称</param>
        /// <returns></returns>
        public static string GetDataTypeName (string typeName)
        {
            //System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]
            //获取 System.Int32
            if (typeName.IndexOf("null", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                return GetBetweenStr(typeName, "[[", ",") ;
            }

            return typeName; 
        }

        /// <summary>
        /// 获取两个字符串中间的字符串
        /// </summary>
        /// <param name="str">要处理的字符串,例ABCD</param>
        /// <param name="str1">第1个字符串,例AB</param>
        /// <param name="str2">第2个字符串,例D</param>
        /// <param name="isIgnoreCase">是否忽略大小</param>
        /// <returns>例返回C</returns>
        public static string GetBetweenStr(string str, string str1, string str2, bool isIgnoreCase = true)
        {
            int i1 = isIgnoreCase ? str.IndexOf(str1, StringComparison.InvariantCultureIgnoreCase) : str.IndexOf(str1);
            if (i1 < 0) //找不到返回空
            {
                return "";
            }

            int i2 = isIgnoreCase ? str.IndexOf(str2, i1 + str1.Length, StringComparison.InvariantCultureIgnoreCase) : str.IndexOf(str2, i1 + str1.Length); //从找到的第1个字符串后再去找
            if (i2 < 0) //找不到返回空
            {
                return "";
            }

            return str.Substring(i1 + str1.Length, i2 - i1 - str1.Length);
        }

        /// <summary>
        /// 生成实体生成代码
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string GetModelCode(List<DbColumnInfo> list)
        {
            StringBuilder sb = new StringBuilder() ;
            for (int i = 0; i < list.Count; i++)
            {
                sb.AppendLine("        ///<summary>");
                sb.AppendLine("        ///" + list[i].ColumnDescription);
                sb.AppendLine("        ///</summary>");
                sb.AppendLine("        public "+ list[i].DataType + " " + (list[i].DbColumnName) + " { get; set; }");
                sb.AppendLine("");
            }

            return sb.ToString() ;
        }
    }
}

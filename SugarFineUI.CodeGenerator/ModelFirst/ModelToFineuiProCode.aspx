﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModelToFineuiProCode.aspx.cs" Inherits="SugarFineUI.CodeGenerator.ModelFirst.ModelToFineuiProCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ModelFirst生成fineui代码</title>  
    <link href="/Scripts/styles/vs.css" rel="stylesheet" />
</head>
<body>
<script src="/Scripts/highlight.pack.js"></script>
<script>hljs.configure({useBR: true});/*使用code的自动换行，highlight.js不自行增加换行*/</script>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="Panel1" />
        <f:Panel ID="Panel1" CssClass="blockpanel" Margin="5px" runat="server" ShowBorder="false" ShowHeader="false" Layout="Region">
            <items>
                <f:Panel ID="Panel5" runat="server" ShowBorder="True" Layout="VBox" AutoScroll="true" ShowHeader="True" Title="反射DLL的Enties生成FineuiPro代码。"
            BoxConfigChildMargin="0 0 5 0" BodyPadding="5">
                    <items>
                        <f:SimpleForm ID="SimpleForm1" Title="面板1" Height="150px" runat="server"
                            BodyPadding="5px" ShowBorder="true" ShowHeader="false" Layout="Fit" AutoScroll="True">
                            <Toolbars>
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:TextBox runat="server" Label="命名空间" ID="txb_NameSpace" ShowRedStar="True" LabelAlign="Right" Text="SugarFineUI.Web"/>
                                        <f:TextBox runat="server" Label="项目路径" ID="txb_Path" Text="Admin" LabelWidth="140" EmptyText="例: admin/news" ShowRedStar="True"  LabelAlign="Right" />
                                    </Items>
                                </f:Toolbar>
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:TextBox runat="server" Label="页面继承类名" ID="txb_BaseClassName" Text="BasePage" ShowRedStar="True" LabelWidth="130" LabelAlign="Right" />
                                        <f:TextBox runat="server" Label="Sugar实体空间" ID="txb_ModelName" Text="SugarFineUI.Enties" ShowRedStar="True" LabelWidth="130" LabelAlign="Right" />
                                        
                                    </Items>
                                </f:Toolbar>
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:TextBox runat="server" ID="txb_savePath" Label="代码存放位置" Text="C:\Code" Required="True" ShowRedStar="True" LabelWidth="140px"/>
                                        <f:ToolbarSeparator runat="server" />
                                        <f:CheckBox runat="server" Label="是否带view页面" ID="cb_isHaveViewPage" LabelWidth="130" />
                                        <f:ToolbarSeparator runat="server" />
                                        <f:Button runat="server" ID="Btn_SetSimpleCode" Text="生成简单代码" Icon="PageWhiteCsharp" OnClick="Btn_SetSimpleCode_OnClick" OnClientClick="if(!validate())return ;" ValidateForms="SimpleForm1" />
                                   </Items>
                                </f:Toolbar>
                            </Toolbars>
                        </f:SimpleForm>
                        <f:TabStrip ID="tab2" BoxFlex="1" Margin="0" IsFluid="true" EnableTabCloseMenu="false"
                            runat="server" BodyPadding="5px" ShowBorder="true">
                            <Toolbars>   
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:Button runat="server" ID="btn_ShowCode" Text="显示单页代码" Icon="PageWhiteCsharp" OnClick="btn_ShowCode_OnClick" OnClientClick="if(!validate())return ;"  />
                                    </Items>
                                </f:Toolbar>
                            </Toolbars>
                            <Tabs>
                                <f:Tab Title="Add.aspx" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_addAspx" EncodeText="False" />
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="Add.cs" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_addCS" EncodeText="False"/>
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="List.aspx" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_listAspx" EncodeText="False"/>
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="list.cs" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_listCS" EncodeText="False"/>
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="View.aspx" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_viewAspx" EncodeText="False"/>
                                    </Items>
                                </f:Tab>
                                <f:Tab Title="View.cs" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_viewCS" EncodeText="False"/>
                                    </Items>
                                </f:Tab>
                            </Tabs>
                        </f:TabStrip>

                    </items>

                </f:Panel> 
                <f:Panel runat="server" ID="panelRightRegion" RegionPosition="Right" RegionSplit="true" EnableCollapse="true" 
                         Width="200px" Title="反射类库" Layout="VBox" ShowBorder="true" ShowHeader="true" BodyPadding="5px" IconFont="_PullRight">
                    <Items>
                        <f:TextBox ID="ttbDB" runat="server" ShowLabel="false" EmptyText="dll的物理地址" ColumnWidth="100%" />
                        <f:TextBox ID="txb_DbXml" runat="server" ShowLabel="false" EmptyText="XML注释文件地址" ColumnWidth="100%" />
                        <f:Panel runat="server" ShowBorder="False" ShowHeader="False" Height="42px">
                            <Items>
                                <f:Button runat="server" Icon="PageWhiteDatabaseYellow" ID="btn_DBlink" Size="Normal" OnClick="btn_DBlink_OnClick" Text="反射DLL和注释文件"></f:Button>
                            </Items>
                        </f:Panel>
                        <f:TwinTriggerBox ID="ttbSearchTableName" runat="server" ShowLabel="false" EmptyText="表名搜索" OnTrigger1Click="ttbSearchTableName_OnTrigger1Click" OnTrigger2Click="ttbSearchTableName_OnTrigger2Click"
                                          Trigger1Icon="Clear" Trigger2Icon="Search" ShowTrigger1="false" />
                        <f:Panel runat="server" Layout="HBox" ShowBorder="False" ShowHeader="False">
                            <Items>
                                <f:Button runat="server" Text="全选" Icon="TableMultiple" EnablePostBack="False" OnClientClick="setallcheck(true);" MarginLeft="2px"/>
                                <f:Button runat="server" Text="都不选" Icon="TableDelete" EnablePostBack="False" OnClientClick="setallcheck(false);" MarginLeft="2px"/>
                            </Items>
                        </f:Panel>
                        <f:CheckBoxList runat="server" ID="cbl_tables" ColumnNumber="1"></f:CheckBoxList>
                    </Items>
                </f:Panel>
             </items>
        </f:Panel>
        <f:HiddenField runat="server" ID="hd_tables" />
    </form>
</body>
</html>
<script>
    var baseClassName;
    var nameSpace;
    var path;
    var modelName;
    var savePath;

    var hd_tables;

    F.ready(function () {
        baseClassName = F('<%=txb_BaseClassName.ClientID%>');
        nameSpace = F('<%=txb_NameSpace.ClientID%>');
        path = F('<%=txb_Path.ClientID%>');
        modelName = F('<%=txb_ModelName.ClientID%>');
        savePath = F('<%=txb_savePath.ClientID%>');
        hd_tables = F('<%=hd_tables.ClientID%>');
    });

    //表单验证。由于toolbar里不能进行验证所以要js验证
    function validate() {
        var error = "";
        if (baseClassName.getValue().length == 0) {
            baseClassName.markInvalid(temp);
            error += "请填写[页面继承类名]<br/>";
        }
        if (nameSpace.getValue().length == 0) {
            nameSpace.markInvalid("请填写[命名空间]");
            error += "请填写[命名空间]<br/>";
        }
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[项目路径]");
            error += "请填写[项目路径]<br/>";
        }

        if (modelName.getValue().length == 0) {
            modelName.markInvalid("请填写[Sugar实体空间]");
            error += "请填写[Sugar实体空间]<br/>";
        }

        if (savePath.getValue().length == 0) {
            savePath.markInvalid("请填写[代码存放位置]");
            error += "请填写[代码存放位置]<br/>";
        }

        //获取表名称
        //var tables = $('#Panel1_panelRightRegion iframe')[0].contentWindow.GetTables();
        var tables = GetTables();
        if (tables.length == 0) {
            error += "请在右侧[反射类库]中，选中目标表！";
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        //取表名称值
        hd_tables.setValue(tables);

        baseClassName.clearInvalid();
        nameSpace.clearInvalid();
        path.clearInvalid();
        modelName.clearInvalid();
        savePath.clearInvalid();
        return true;
    }

    function validate_path() {
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[项目路径]");
            F.alert("请填写[项目路径]");
            return false;
        } else {
            path.clearInvalid();
            return true;
        }
    }
</script>

<script>
    function RefreshTables() {
        __doPostBack('btn_RefreshTables', '');
    }

    //设置全选或全不选
    function setallcheck(isCheck) {
        var array = F('<%=cbl_tables.ClientID%>').items;
        array.forEach(function (item) {
            item.setValue(isCheck);
        });
    }

    function GetTables() {
        var selectedValues = F('<%=cbl_tables.ClientID%>').getValue().join(',');
        return selectedValues;
    }

    function ShowHighlight() {
        $('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
        });
    }
</script>
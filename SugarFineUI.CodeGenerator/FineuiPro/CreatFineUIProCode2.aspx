﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatFineUIProCode.aspx.cs" Inherits="BoYuan.CodeGenerator.CreatFineUIProCode2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="Panel5" />
        <f:Panel ID="Panel5" runat="server" ShowBorder="True"
            Layout="VBox" AutoScroll="true"
            ShowHeader="True" Title="自动生成Fineui代码。生成代码前注意做好备份以免代码被覆盖 "
            BoxConfigChildMargin="0 0 5 0" BodyPadding="5">
            <Items>

                <f:SimpleForm ID="SimpleForm1" Title="面板1" Height="300px" runat="server"
                    BodyPadding="5px" ShowBorder="true" ShowHeader="false" Layout="Fit" AutoScroll="True">
                    <Toolbars>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:TextBox runat="server" Label="命名空间" ID="txb_NameSpace" ShowRedStar="True" LabelAlign="Right" />
                                <f:TextBox runat="server" Label="存放项目中的路径" ID="txb_Path" Text="Admin" LabelWidth="140" EmptyText="例: admin/news" ShowRedStar="True"  LabelAlign="Right" />
                                <f:TextBox runat="server" Label="页面继承类名" ID="txb_BaseClassName" Text="BasePage" ShowRedStar="True" LabelWidth="130" LabelAlign="Right" />
                            </Items>
                        </f:Toolbar>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:CheckBox runat="server" Label="是否带view页面" ID="cb_isHaveViewPage" LabelWidth="130" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button runat="server" ID="Btn_SetSimpleCode" Text="生成简单代码" Icon="PageWhiteCsharp" OnClick="Btn_SetSimpleCode_OnClick" OnClientClick="if(!validate())return ;" ValidateForms="SimpleForm1" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button runat="server" ID="Btn_SetRelationCode" Text="生成配置代码" Icon="PageWhiteDatabaseYellow" OnClick="Btn_SetRelationCode_OnClick" OnClientClick="if(!validate())return ;" ValidateForms="SimpleForm1" />
                                <f:ToolbarFill runat="server" />
                                <f:Button runat="server" Text="全选" Icon="TableMultiple" EnablePostBack="False" OnClientClick="setallcheck(true);" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button runat="server" Text="都不选" Icon="TableDelete" EnablePostBack="False" OnClientClick="setallcheck(false);" />
                                <f:ToolbarFill runat="server" />
                                <f:Button runat="server" ID="Btn_AddPageInfoToDB" Text="添加页面到数据库" ConfirmText="确认要添加到数据库中？" Icon="DatabaseAdd" OnClick="Btn_AddPageInfoToDB_OnClick" OnClientClick="if(!validate_path())return;" />
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                    <Items>
                        <f:CheckBoxList ID="cbl_tableNames" Label="选择业务表" ColumnNumber="5" runat="server" ShowRedStar="true" Required="True">
                        </f:CheckBoxList>
                    </Items>
                </f:SimpleForm>
                <f:SimpleForm ID="SimpleForm2" Title="设置关联" BoxFlex="1" Margin="0"
                    runat="server" BodyPadding="5px" ShowBorder="true" ShowHeader="true" Layout="Fit">

                    <Toolbars>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:DropDownList runat="server" ID="ddl_tableName" Label="表名" LabelWidth="120" AutoPostBack="True" EnableEdit="true" ForceSelection="True" OnSelectedIndexChanged="ddl_tableName_OnSelectedIndexChanged">
                                </f:DropDownList>
                                <f:DropDownList runat="server" ID="ddl_column" Label="字段" AutoPostBack="True" EnableEdit="true" ForceSelection="True">
                                </f:DropDownList>
                                <f:DropDownList runat="server" ID="ddl_type" Label="设置类别" AutoPostBack="True" OnSelectedIndexChanged="ddl_type_OnSelectedIndexChanged">
                                </f:DropDownList>
                            </Items>
                        </f:Toolbar>
                        <f:Toolbar runat="server" ID="tlb_bind">
                            <Items>
                                <f:DropDownList runat="server" ID="ddl_tableName2" Label="关联外键表名" LabelWidth="120" AutoPostBack="True" EnableEdit="true" ForceSelection="True" OnSelectedIndexChanged="ddl_tableName2_OnSelectedIndexChanged">
                                </f:DropDownList>
                                <f:DropDownList runat="server" ID="ddl_column2" Label="显示字段" AutoPostBack="True" EnableEdit="true" ForceSelection="True">
                                </f:DropDownList>
                                <f:Button runat="server" ID="btn_Relation" Text="设置关联" OnClick="btn_Relation_OnClick"></f:Button>
                            </Items>
                        </f:Toolbar>

                        <f:Toolbar runat="server" ID="tlb_simple" Hidden="True">
                            <Items>
                                <f:Button runat="server" ID="btn_setKindEditor" Text="设置为富文本KindEditor" OnClick="btn_setKindEditor_OnClick"></f:Button>
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                    <Items>

                        <f:Grid ID="Grid1" runat="server" AllowPaging="True" AllowSorting="True" SortDirection="asc" SortField="sort" PageSize="10"
                            EnableCheckBoxSelect="true" AutoScroll="True" DataKeyNames="id" EnableTextSelection="True" CheckBoxSelectOnly="True"
                            IsDatabasePaging="True" ShowHeader="False" OnPageIndexChange="Grid1_OnPageIndexChange">

                            <PageItems>
                                <f:ToolbarSeparator runat="server" />

                                <f:ToolbarText runat="server" Text="每页记录数：10" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button ID="Button_delete" runat="server" Icon="Delete" Text="批量删除"
                                    ConfirmText="确定要删除选中的数据吗？" OnClick="Button_delete_OnClick" />

                            </PageItems>
                            <Columns>
                                <f:BoundField runat="server" Width="100px" DataField="tablename1" HeaderText="表名" />
                                <f:BoundField runat="server" Width="100px" DataField="columnkey" HeaderText="字段" />
                                <f:BoundField runat="server" Width="100px" DataField="ctrltype" HeaderText="设置类别" />
                                <f:BoundField runat="server" Width="100px" DataField="tablename2" HeaderText="关联表名" />
                                <f:BoundField runat="server" Width="100px" DataField="column2" HeaderText="显示字段" />
                                <f:BoundField runat="server" Width="100px" DataField="key2" HeaderText="主键表主键" />


                            </Columns>
                        </f:Grid>
                    </Items>
                </f:SimpleForm>

            </Items>

        </f:Panel>

    </form>
</body>
</html>

<script>

    var baseClassName;
    var nameSpace;
    var path;


    F.ready(function () {
        //初始化 获取对象。只能在F.ready里获取到ext对象
        baseClassName = F('<%=txb_BaseClassName.ClientID%>');
        nameSpace = F('<%=txb_NameSpace.ClientID%>');
        path = F('<%=txb_Path.ClientID%>');
    });

    function validate_path() {
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[存放项目中的路径]");
            F.alert("请填写[存放项目中的路径]");
            return false;
        } else {
            path.clearInvalid();
            return true;
        }
    }

    //表单验证。由于toolbar里不能进行验证所以要js验证
    function validate() {

        var error = "";
        if (baseClassName.getValue().length == 0) {
            var temp = "请填写[页面继承类名]<br/>";
            baseClassName.markInvalid(temp);
            error += temp;
        }
        if (nameSpace.getValue().length == 0) {
            var temp = "请填写[命名空间]<br/>";
            nameSpace.markInvalid("请填写[命名空间]");
            error += temp;
        }
        if (path.getValue().length == 0) {
            var temp = "请填写[存放项目中的路径]<br/>";
            path.markInvalid("请填写[存放项目中的路径]");
            error += temp;
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        baseClassName.clearInvalid();
        nameSpace.clearInvalid();
        path.clearInvalid();
        return true;
    }

    //设置全选或全不选
    function setallcheck(isCheck) {
        var array = F('<%=cbl_tableNames.ClientID%>').items;
        array.forEach(function (item) {
            item.setValue(isCheck);
        });
    }


</script>

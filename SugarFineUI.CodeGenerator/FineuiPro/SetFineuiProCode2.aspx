﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetFineuiProCode2.aspx.cs" Inherits="BoYuan.CodeGenerator.FineuiPro.SetFineuiProCode2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="Panel5" />
        <f:Panel ID="Panel5" runat="server" ShowBorder="True" Layout="VBox" AutoScroll="true" ShowHeader="True" Title="自动生成FineuiPro代码。"
            BoxConfigChildMargin="0 0 5 0" BodyPadding="5">
            <items>
                <f:SimpleForm ID="SimpleForm1" Title="面板1" Height="300px" runat="server"
                    BodyPadding="5px" ShowBorder="true" ShowHeader="false" Layout="Fit" AutoScroll="True">
                    <Toolbars>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:TextBox runat="server" Label="命名空间" ID="txb_NameSpace" ShowRedStar="True" LabelAlign="Right" Text="BoYuan"/>
                                <f:TextBox runat="server" Label="项目路径" ID="txb_Path" Text="Admin" LabelWidth="140" EmptyText="例: admin/news" ShowRedStar="True"  LabelAlign="Right" />
                                <f:TextBox runat="server" Label="页面继承类名" ID="txb_BaseClassName" Text="BasePage" ShowRedStar="True" LabelWidth="130" LabelAlign="Right" />
                                <f:TextBox runat="server" Label="Orm实体空间" ID="txb_ModelName" Text="BoYuan.Enties" ShowRedStar="True" LabelWidth="130" LabelAlign="Right" />
                                
                            </Items>
                        </f:Toolbar>
                        <f:Toolbar runat="server">
                            <Items>
                                <f:TextBox runat="server" ID="txb_savePath" Label="代码存放位置" Text="C:\Code" Required="True" ShowRedStar="True" LabelWidth="140px"/>
                                <f:ToolbarSeparator runat="server" />
                                <f:CheckBox runat="server" Label="是否带view页面" ID="cb_isHaveViewPage" LabelWidth="130" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button runat="server" ID="Btn_SetSimpleCode" Text="生成简单代码" Icon="PageWhiteCsharp" OnClick="Btn_SetSimpleCode_OnClick" OnClientClick="if(!validate())return ;" ValidateForms="SimpleForm1" />
                                <f:ToolbarSeparator runat="server" />
                                <f:Button runat="server" ID="Btn_AddPageInfoToDB" Text="添加页面到数据库" ConfirmText="确认要添加到数据库中？" Icon="DatabaseAdd" OnClick="Btn_AddPageInfoToDB_OnClick" OnClientClick="if(!validate_path())return;" />
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                </f:SimpleForm>
                <f:TabStrip ID="tab2" BoxFlex="1" Margin="0" IsFluid="true" EnableTabCloseMenu="false"
                    runat="server" BodyPadding="5px" ShowBorder="true">
                    <Toolbars>   
                        <f:Toolbar runat="server">
                            <Items>
                                <f:Button runat="server" ID="btn_ShowCode" Text="显示单页代码" Icon="PageWhiteCsharp" OnClick="btn_ShowCode_OnClick" OnClientClick="if(!validate())return ;"  />
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                    <Tabs>
                        <f:Tab Title="Add.aspx" BodyPadding="10px" Layout="Fit" runat="server">
                            <Items>
                                <f:Label runat="server" ID="lb_addAspx" />
                            </Items>
                        </f:Tab>
                        <f:Tab Title="Add.cs" BodyPadding="10px" Layout="Fit" runat="server">
                            <Items>
                            </Items>
                        </f:Tab>
                        <f:Tab Title="List.aspx" BodyPadding="10px" Layout="Fit" runat="server">
                            <Items>
                            </Items>
                        </f:Tab>
                        <f:Tab Title="list.cs" BodyPadding="10px" Layout="Fit" runat="server">
                            <Items>
                            </Items>
                        </f:Tab>
                        <f:Tab Title="View.aspx" BodyPadding="10px" Layout="Fit" runat="server">
                            <Items>
                            </Items>
                        </f:Tab>
                        <f:Tab Title="View.cs" BodyPadding="10px" Layout="Fit" runat="server">
                            <Items>
                            </Items>
                        </f:Tab>
                    </Tabs>
                </f:TabStrip>

            </items>

        </f:Panel>
        <f:HiddenField runat="server" ID="hd_tables" />
    </form>
</body>
</html>
<script>
    var baseClassName;
    var nameSpace;
    var path;
    var modelName;
    var savePath;

    var hd_tables;

    F.ready(function () {
        baseClassName = F('<%=txb_BaseClassName.ClientID%>');
        nameSpace = F('<%=txb_NameSpace.ClientID%>');
        path = F('<%=txb_Path.ClientID%>');
        modelName = F('<%=txb_ModelName.ClientID%>');
        savePath = F('<%=txb_savePath.ClientID%>');
        hd_tables = F('<%=hd_tables.ClientID%>');
    });

    //表单验证。由于toolbar里不能进行验证所以要js验证
    function validate() {
        var error = "";
        if (baseClassName.getValue().length == 0) {
            baseClassName.markInvalid(temp);
            error += "请填写[页面继承类名]<br/>";
        }
        if (nameSpace.getValue().length == 0) {
            nameSpace.markInvalid("请填写[命名空间]");
            error += "请填写[命名空间]<br/>";
        }
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[项目路径]");
            error += "请填写[项目路径]<br/>";
        }

        if (modelName.getValue().length == 0) {
            modelName.markInvalid("请填写[Orm实体空间]");
            error += "请填写[Orm实体空间]<br/>";
        }

        if (savePath.getValue().length == 0) {
            savePath.markInvalid("请填写[代码存放位置]");
            error += "请填写[代码存放位置]<br/>";
        }

        //获取表名称
        var tables = parent.GetTables();
        if (tables.length == 0) {
            error += "请选中要生成的代码的表格！";
        }

        if (error.length > 0) {
            F.alert(error);
            return false;
        }

        //取表名称值
        hd_tables.setValue(tables);

        baseClassName.clearInvalid();
        nameSpace.clearInvalid();
        path.clearInvalid();
        modelName.clearInvalid();
        savePath.clearInvalid();
        return true;
    }

    function validate_path() {
        if (path.getValue().length == 0) {
            path.markInvalid("请填写[项目路径]");
            F.alert("请填写[项目路径]");
            return false;
        } else {
            path.clearInvalid();
            return true;
        }
    }


</script>

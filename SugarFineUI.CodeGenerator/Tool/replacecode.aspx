﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="replacecode.aspx.cs" Inherits="SugarFineUI.CodeGenerator.Tool.replacecode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>替换代码</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="form_Edit" FormLabelWidth="140" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title="ABCDEFG，替换字符串CD为123，  最终变成 AB123EFG " runat="server" AutoScroll="true"
            EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server" ColumnWidth="50% 50%">
                    <Items>
                        <f:TextBox runat="server" ID="txb_replace" Label="被替换的字符串"  ShowRedStar="True" />
                        <f:TextBox runat="server" ID="txb_newstr" Label="替换成(字符串)" />

                    </Items>
                </f:FormRow>



                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Height="300px" Required="True" ShowRedStar="True" Label="批量内容"/>
                    </Items>
                </f:FormRow>

                <f:FormRow runat="server">
                    <Items>
                        <f:Button runat="server" Text="转到上方↑" OnClientClick=" codeUP();return ;" EnableAjax="False" EnablePostBack="False" DisableControlBeforePostBack="False" />

                    </Items>
                </f:FormRow>
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_new" Height="300px" Label="新内容" Readonly="True" />
                    </Items>
                </f:FormRow>

            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="替换代码" Icon="Html" OnClick="btn_showCode_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                        <f:Button runat="server"  ID="btn_delNullLine" Text="删除空行" Icon="PageWhiteText" OnClick="btn_delNullLine_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                        <f:ToolbarText runat="server" Text="ABCDEFG，替换字符串CD为123，  最终变成 AB123EFG " />
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>
</body>
</html>
<script>
    function codeUP() {
        F('<%=txa_old.ClientID%>').setValue(F('<%=txa_new.ClientID%>').getValue());
        F('<%=txa_new.ClientID%>').setValue("");
    }
</script>

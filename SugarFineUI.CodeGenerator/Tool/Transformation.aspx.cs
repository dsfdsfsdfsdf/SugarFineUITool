﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro;


namespace SugarFineUI.CodeGenerator.Tool
{
    public partial class Transformation : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {

            string[] data = txa_old.Text.Split(Environment.NewLine.ToCharArray());

            StringBuilder sb=new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.AppendLine(GetExchangeString(data[i]));
            }

            txa_new.Text = sb.ToString();
        }

        private string GetExchangeString(string str)
        {
            bool haveSemicolon = str.Contains(txb_Semicolon.Text);

            if (haveSemicolon)//是否有结尾符号
            {
                str = str.Replace(txb_Semicolon.Text, string.Empty);
            }

            char[] a = txb_Split.Text.ToCharArray();
            string[] strings = str.Split(a);

            if (strings.Length > 1)
            {
                return string.Format("{0} = {1}{2}", strings[1], strings[0], haveSemicolon ? txb_Semicolon.Text : string.Empty);
            }

            return str;
        }
    }
}
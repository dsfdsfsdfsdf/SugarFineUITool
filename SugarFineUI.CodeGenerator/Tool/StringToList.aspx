﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StringToList.aspx.cs" Inherits="SugarFineUI.CodeGenerator.Tool.StringToList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>转多行</title>
</head>
<body>
    <form id="form1" runat="server">

        <f:PageManager runat="server" AutoSizePanelID="form_Edit" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title="" runat="server" AutoScroll="true"
            EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server" ColumnWidth="50% 50%">
                    <Items>
                        <f:TextBox runat="server" ID="txb_Split" Label="分割符号" Text="," Required="True" ShowRedStar="True" />
                        <f:TextBox runat="server" ID="txb_Semicolon" Label="每行加尾字符"  LabelWidth="120px"   />
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server">
                    <Items>
                        <f:TextBox runat="server" ID="txb_string" Required="True" ShowRedStar="True" Label="字符串" />
                    </Items>
                </f:FormRow>
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_List" Height="300px" Label="字符串集合" Readonly="True"/>
                    </Items>
                </f:FormRow>

            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="分割转多行" Icon="Html" OnClick="btn_showCode_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                        <f:ToolbarText runat="server" Text="把“a,b,c”,分割成 a b c" />
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>

</body>
</html>

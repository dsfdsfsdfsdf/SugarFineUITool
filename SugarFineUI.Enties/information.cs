﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace SugarFineUI.Enties
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("information")]
    public partial class information
    {
           public information(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string title {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string info {get;set;}

           /// <summary>
           /// Desc:
           /// Default:1000
           /// Nullable:True
           /// </summary>           
           public int? sort {get;set;}

           /// <summary>
           /// Desc:分类
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? typenum {get;set;}

    }
}

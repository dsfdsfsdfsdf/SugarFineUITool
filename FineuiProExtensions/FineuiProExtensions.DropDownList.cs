﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using SugarFineUI.DBServices ;
using FineUIPro;


namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        /// <param name="dll">控件</param>
        /// <param name="tableName">表名称</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void Bind(this FineUIPro.DropDownList dll, string tableName, string textName, string valueName)
        {
            Bind(dll, tableName, textName, valueName, string.Empty);
        }

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        /// <param name="dll">控件</param>
        /// <param name="tableName">表名称</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        /// <param name="whereSql">sql条件，注意会有注入危险(不用加where)</param>
        public static void Bind(this FineUIPro.DropDownList dll, string tableName, string textName, string valueName, string whereSql)
        {
            string sql = string.Format("select {0},{1} from {2} {3} ", textName, valueName, tableName, (whereSql.Length > 0 ? " where " + whereSql : string.Empty));

            //DataSet ds = DBServices.Query(sql);

            //if (ds.Tables[0].Rows.Count > 0)
            //{

            //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //    {
            //        dll.Items.Add(new FineUIPro.ListItem(ds.Tables[0].Rows[i][textName].ToString(), ds.Tables[0].Rows[i][valueName].ToString()));
            //    }
            //}

            var db = DB_Base.Instance ;
            //以下方法 会给textName 添加\r\n，导致不能正常取值，原因不清楚。
            BindByDataSource(dll, db.Ado.GetDataSetAll(sql), textName, valueName);

            //在业务逻辑里设置强制选中
            //if (dll.Items != null && dll.Items.Count > 0) dll.Items[0].Selected = true;
        }

        /// <summary>
        /// 绑定枚举类型到下拉框 //Bind(newstype, typeof (EnumClass .NewsType));
        /// </summary>
        /// <param name="dll"> 下拉框</param>
        /// <param name="enumType"> 枚举类型</param>
        public static void Bind(this FineUIPro.DropDownList dll, Type enumType)
        {
            foreach (int i in Enum.GetValues(enumType))
            {
                dll.Items.Add(new FineUIPro.ListItem(Enum.GetName(enumType, i), i.ToString()));
            }
        }

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        /// <param name="dll">控件</param>
        /// <param name="ds">数据源</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void BindByDataSource(this FineUIPro.DropDownList dll, object ds, string textName, string valueName)
        {
            dll.DataTextField = textName;
            dll.DataValueField = valueName;
            dll.DataSource = ds;
            dll.DataBind();
        }

        /// <summary>
        /// 获取可编辑下拉框的文本值
        /// </summary>
        /// <param name="dll"></param>
        /// <returns></returns>
        public static string GetEditText(this FineUIPro.DropDownList dll)
        {
            //注意，如果dll的item是null(一个item都没有绑定)，dll.Text的值永远是空字符串
            return dll.SelectedItem != null ? dll.SelectedItem.Text : dll.Text;
        }


        /// <summary>
        /// 获取多选值
        /// </summary>
        /// <param name="dll"></param>
        public static List<string> GetSelectionValues(this FineUIPro.DropDownList dll)
        {
            if (dll.SelectedItem != null)
            {
                //List<string> texts = new List<string>();
                List<string> values = new List<string>();
                foreach (ListItem item in dll.SelectedItemArray)
                {
                    //texts.Add(item.Text);
                    values.Add(item.Value);
                }

                return values;

               
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获取多选文本
        /// </summary>
        /// <param name="dll"></param>
        public static List<string> GetSelectionTests(this FineUIPro.DropDownList dll)
        {
            if (dll.SelectedItem != null)
            {
                List<string> texts = new List<string>();
                //List<string> values = new List<string>();
                foreach (ListItem item in dll.SelectedItemArray)
                {
                    texts.Add(item.Text);
                    //values.Add(item.Value);
                }

                return texts;

               
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据值集合设置多选状态
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="list"></param>
        public static void SetSelectionByValues(this FineUIPro.DropDownList ddl,IEnumerable<string> list)
        {
            foreach (var v in list)
            {
                foreach (var t in ddl.Items)
                {
                    if (t.Value == v)
                    {
                        t.Selected =true;
                    }
                    //不要设置t.Selected = false; 否则会导致多选中项失效
                    //else
                    //{
                    //    t.Selected = false;
                    //}

                }
            }
        }


    }

}

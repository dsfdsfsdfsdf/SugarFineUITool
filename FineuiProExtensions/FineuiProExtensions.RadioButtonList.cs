﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineUIPro;
 

namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {

        /// <summary>
        /// 绑定单选框列表
        /// </summary>
        /// <param name="rbl">控件</param>
        /// <param name="tableName">表名称</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void Bind(this FineUIPro.RadioButtonList rbl, string tableName, string textName, string valueName)
        {
            Bind(rbl, tableName, textName, valueName, string.Empty);
        }

        /// <summary>
        /// 绑定单选框列表
        /// </summary>
        /// <param name="rbl">控件</param>
        /// <param name="tableName">表名称</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        /// <param name="whereSql">sql条件，注意会有注入危险(不用加where)</param>
        public static void Bind(this FineUIPro.RadioButtonList rbl, string tableName, string textName, string valueName, string whereSql)
        {
            string sql = string.Format("select {0},{1} from {2} {3} ", textName, valueName, tableName, (whereSql.Length > 0 ? " where " + whereSql : string.Empty));
            var db = SugarFineUI.DBServices.DB_Base.Instance ;
            BindByDataSource(rbl, db.Ado.GetDataTable(sql), textName, valueName);
        }

        /// <summary>
        /// 绑定枚举类型到单选框 //Bind(newstype, typeof (EnumClass .NewsType));
        /// </summary>
        /// <param name="rbl"> 单选框</param>
        /// <param name="enumType"> 枚举类型</param>
        public static void Bind(this FineUIPro.RadioButtonList rbl, Type enumType)
        {
            foreach (int i in Enum.GetValues(enumType))
            {
                rbl.Items.Add(new FineUIPro.RadioItem(Enum.GetName(enumType, i), i.ToString()));
            }
        }

        /// <summary>
        /// 绑定单选框
        /// </summary>
        /// <param name="rbl">控件</param>
        /// <param name="ds">数据源</param>
        /// <param name="textName">文本字段</param>
        /// <param name="valueName">值字段</param>
        public static void BindByDataSource(this FineUIPro.RadioButtonList rbl, object ds, string textName, string valueName)
        {
            rbl.DataTextField = textName;
            rbl.DataValueField = valueName;
            rbl.DataSource = ds;
            rbl.DataBind();
        }
    }

}


